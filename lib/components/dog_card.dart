import 'package:flutter/material.dart';
import 'package:we_rate_dogs/model/dog_model.dart';
import 'package:we_rate_dogs/screens/dog_detail_screen.dart';

class DogCard extends StatefulWidget {
  final Dog dog;
  DogCard(this.dog);

  @override
  _DogCardState createState() => _DogCardState(dog);
}

class _DogCardState extends State<DogCard> {
  Dog dog;
  String renderUrl;

  _DogCardState(this.dog);
  
  void initState() {
    super.initState();
    renderDogPic();
  }

  void renderDogPic() async {
    await dog.getImageUrl();

    if (mounted) {
      setState(() {
        renderUrl = dog.imageUrl;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: showDogDetailScreen,
      child: Container(
        height: 115.0,
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 66.0,
              child: dogCard,
            ),
            Positioned(
              top: 7.5,
              left: 16,
              child: dogImage)
          ],
        )
      )
    );
  }

  void showDogDetailScreen() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return DogDetailScreen(dog);
        }
      )
    );
  }

  Widget get dogImage {
    return Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(renderUrl ?? '')
        ) 
      ),
    );
  }

  Widget get dogCard {
    return Container(
      width: 274.0,
      height: 115.0,
      child: Card(
        color: Colors.black87,
        child: Padding(
          padding: const EdgeInsets.only(
            top: 8.0,
            bottom: 8.0,
            left: 64.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(widget.dog.name,
                style: Theme.of(context).textTheme.headline),
              Text(widget.dog.location,
                style: Theme.of(context).textTheme.subhead),
              Row(children: <Widget>[
                Icon(Icons.star),
                Text(': ${widget.dog.rating} / 10')
              ],)
            ],

          )
        )
      )
    );
  }

}