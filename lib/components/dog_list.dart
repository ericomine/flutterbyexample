import 'package:flutter/material.dart';

import 'package:we_rate_dogs/components/dog_card.dart';
import 'package:we_rate_dogs/model/dog_model.dart';

class DogList extends StatelessWidget {
  final List<Dog> dogs;
  DogList(this.dogs);

  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }

  ListView _buildList(context) {
    return ListView.builder(
      itemCount: dogs.length,
      itemBuilder: (context, i) {
        return DogCard(dogs[i]);
      },
    );
  }
}