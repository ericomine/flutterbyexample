import 'package:flutter/material.dart';
import 'package:we_rate_dogs/model/dog_model.dart';

class NewDogForm extends StatefulWidget {
  @override
  _NewDogFormState createState() => _NewDogFormState();
}

class _NewDogFormState extends State<NewDogForm> {
  var dogName;
  var dogLocation;
  var dogDescription;
  Dog newDog;

  Widget get fieldPupName {
    return Padding(
      padding:
        const EdgeInsets.symmetric(horizontal: 32.0),
      child: TextField(
        onChanged: (text) => dogName = text,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          labelText: 'Name the pup',
        ),
      ),
    );
  }
  
  Widget get fieldPupLocation {
    return Padding(
      padding:
        const EdgeInsets.symmetric(horizontal: 32.0), 
      child: TextField(
        onChanged: (text) => dogLocation = text,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          labelText: 'Pups location',         
        ),
      ),
    );
  }

  Widget get fieldPupDescription {
    return Padding(
      padding:
        const EdgeInsets.symmetric(horizontal: 32.0),
      child: TextField(
        onChanged: (text) => dogDescription = text,
        textAlign: TextAlign.left,
        decoration: InputDecoration(
          labelText: 'All about the pup',
        ),
      ),
    );
  }

  Widget get btnSubmitForm {
    return Padding(
      padding:
        const EdgeInsets.all(16.0),
      child: RaisedButton(
        color: Colors.indigo,
        child:
          Text('Submit Pup'),
        onPressed: () {
          newDog = Dog(dogName, dogLocation, dogDescription);
          Navigator.pop(context, newDog);
        },
      ),
    );
  }

  Widget get formFields {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        fieldPupName,
        fieldPupLocation,
        fieldPupDescription,
        btnSubmitForm
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add a new Dog'),
      ),
      body: formFields,
    );
  }
}