import 'package:flutter/material.dart';
import 'package:we_rate_dogs/model/dog_model.dart';

class DogDetailScreen extends StatefulWidget {
  final Dog dog;
  DogDetailScreen(this.dog);

  @override
  _DogDetailScreenState createState() =>
    _DogDetailScreenState();
}

class _DogDetailScreenState extends State<DogDetailScreen> {
  final _dogAvatarSize = 150.0;
  var _sliderValue = 10.0;

  Widget get dogImage {
    return Container(
      height: _dogAvatarSize,
      width: _dogAvatarSize,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          const BoxShadow(
            offset: const Offset(1.0, 2.0),
            blurRadius: 2.0,
            spreadRadius: -1.0,
            color: const Color(0x33000000)),
          const BoxShadow(
            offset: const Offset(2.0, 1.0),
            blurRadius: 3.0,
            spreadRadius: 0.0,
            color: const Color(0x24000000)),
          const BoxShadow(
            offset: const Offset(3.0, 1.0),
            blurRadius: 4.0,
            spreadRadius: 2.0,
            color: const Color(0x1F000000)),
        ],
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(widget.dog.imageUrl),
        ),
      ),
    );    
  }
  
  Widget get dogRating {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.star,
          size: 40.0
        ),
        Text('${widget.dog.rating} / 10',
            style: Theme.of(context).textTheme.display2
        ),
      ],
    );
  }

  Widget get changeRating {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Slider(
          value: _sliderValue,
          divisions: 10,
          min: 0.0,
          max: 10.0,
          onChanged: (value) {
            setState(() {
              _sliderValue = value;
            });
          }            
        ),
        RaisedButton(
          color: Colors.indigo,
          child: Text("Update rating"),
          onPressed: () {
            if (_sliderValue < 7) {
              print("don't do it");
            } else {
              setState(() {
                widget.dog.rating = _sliderValue.toInt(); 
              });
            }
          },
        )
      ],
    );
  }

  Widget get dogProfile {
    return 
    Container(
      width: double.infinity,

      padding: const EdgeInsets.all(32.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          stops: [0.1, 0.5, 0.7, 0.9],
          colors: [
            Colors.indigo[800],
            Colors.indigo[700],
            Colors.indigo[600],
            Colors.indigo[400],
          ]
        )
      ),
      child: Column(            
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          dogImage,
          Text(widget.dog.name,
            style: TextStyle(fontSize: 32.0)
          ),
          Text(widget.dog.location,
            style: TextStyle(fontSize: 20.0)
          ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(widget.dog.description,
              style: Theme.of(context).textTheme.subhead,
            ),
          ),
          dogRating,
          changeRating
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text('Meet ${widget.dog.name}'),
      ),
      body: dogProfile
    );
  }
}